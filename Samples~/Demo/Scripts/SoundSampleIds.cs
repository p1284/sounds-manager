﻿namespace Gameready.Gameplay.SoundsManager.Samples.Data
{
    public static class SoundSampleIds
    {
        public const string Click = "click";
        public const string Menu_back_1 = "menu_back_1";
        public const string Menu_back_2 = "menu_back_2";
        public const string Shoot = "shoot";
    }
}
