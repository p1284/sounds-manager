using System.Collections;
using Gameready.Gameplay.SoundsManager.Data;
using Gameready.Gameplay.SoundsManager.Music;
using Gameready.Gameplay.SoundsManager.Samples.Data;
using UnityEngine;
using UnityEngine.UI;

namespace Gameready.Gameplay.SoundsManager.Samples
{
    public class MusicSampleController : MonoBehaviour
    {
        [SerializeField] private MusicManager _musicManager;

        [SerializeField] private Slider _playbackSlider;
        [SerializeField] private Toggle _muteToggle;
        [SerializeField] private Button _pauseCurrent;

        private bool _playbackUpdate = false;
        private bool _paused;


        private void Start()
        {
            _playbackUpdate = true;
            _playbackSlider.onValueChanged.AddListener(OnPlaybackPosChangedHandler);

            _pauseCurrent.onClick.AddListener(PauseClickHandler);
            _muteToggle.SetIsOnWithoutNotify(_musicManager.Mute);
            _muteToggle.onValueChanged.AddListener(OnMuteChangedHandler);
            _musicManager.Play(MusicSamplesIds.Pleasant_porridge);
        }

        private void OnPlaybackPosChangedHandler(float value)
        {
            _playbackUpdate = false;
            var model = _musicManager.GetCurrentPlayModel();
            if (model != null)
            {
                model.updateTime = value;
            }

            _playbackUpdate = !_paused;
        }

        private void Update()
        {
            if (_playbackUpdate && !string.IsNullOrEmpty(_musicManager.CurrentPlayId))
            {
                var model = _musicManager.GetCurrentPlayModel();
                _playbackSlider.maxValue = model.totalTime;
                _playbackSlider.SetValueWithoutNotify(model.currentTime);
            }
        }

        public void PlaybackSliderDown()
        {
            _playbackUpdate = false;
        }

        public void PlaybackSliderUp()
        {
            _playbackUpdate = true;
        }

        private void PauseClickHandler()
        {
            _paused = !_paused;
            _playbackUpdate = !_paused;
            _pauseCurrent.transform.Find("Text").GetComponent<Text>().text = _paused ? "UnPause" : "Pause";
            _musicManager.Pause(_musicManager.CurrentPlayId, _paused);
        }

        private void OnMuteChangedHandler(bool value)
        {
            _musicManager.Mute = value;
        }

        public void SwitchMusic()
        {
            var model = _musicManager.Config.GetModel(_musicManager.CurrentPlayId);
            StartCoroutine(FadeOutMusic(model));
        }

        private IEnumerator FadeOutMusic(MusicModel model)
        {
            while (model.volume > 0f)
            {
                model.volume -= Time.deltaTime;
                yield return null;
            }

            _musicManager.Stop(model.SoundId);

            _playbackUpdate = false;
            var nextMusic = _musicManager.Config.GetModel(MusicSamplesIds.Voxel_revolution);
            nextMusic.ResetToDefault();
            nextMusic.volume = 0.2f;

            _musicManager.Play(nextMusic);

            _playbackUpdate = true;

            while (nextMusic.volume < 0.9f)
            {
                nextMusic.volume += Time.deltaTime;
                yield return null;
            }
        }
    }
}