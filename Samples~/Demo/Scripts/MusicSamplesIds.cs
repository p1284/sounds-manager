﻿namespace Gameready.Gameplay.SoundsManager.Samples.Data{
    public static class MusicSamplesIds
    {
        public const string Ukulele = "ukulele";
        public const string Pleasant_porridge = "pleasant_porridge";
        public const string Voxel_revolution = "voxel_revolution";
    }
}
