using Gameready.Gameplay.SoundsManager.Samples.Data;
using UnityEngine;
using UnityEngine.UI;

namespace Gameready.Gameplay.SoundsManager.Samples
{
    public class SfxSampleController : MonoBehaviour
    {
        [SerializeField] private Toggle _muteToggle;
        [Range(-1f, 1f)] [SerializeField] private float _randomPitchOffset = 1;
        [SerializeField] private SfxManager _sfxManager;

        private void Start()
        {
            _muteToggle.onValueChanged.AddListener(OnMuteChangedHandler);
            _muteToggle.SetIsOnWithoutNotify(_sfxManager.Mute);
        }

        private void OnMuteChangedHandler(bool value)
        {
            _sfxManager.Mute = value;
        }

        public void Click()
        {
            _sfxManager.Play(SoundSampleIds.Click);
        }

        public void PlayPitched()
        {
            var shootModel = _sfxManager.Config.GetModel(SoundSampleIds.Shoot);
            shootModel.pitch = SoundUtils.GetRandomPitch(shootModel.StartPitch, _randomPitchOffset);
            _sfxManager.Play(shootModel);
        }
    }
}