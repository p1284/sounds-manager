# Changelog

## [1.0.0] - 2022-16-03
 -add SfxManager for sound effects
 -add MusicManager for backsounds
 -add samples for sfx and music with demo scenes