using System.Collections.Generic;
using System.Text;

namespace Gameready.Gameplay.SoundsManager.Editor
{
    public class SoundIdsGenerator
    {
        private readonly string _className;
        private readonly List<string> _clipNames;

        private HashSet<string> _soundIds;

        public SoundIdsGenerator(string className, List<string> clipNames)
        {
            _className = className;
            _clipNames = clipNames;
            _soundIds = new HashSet<string>();
        }

        public string Generate()
        {
            var sb = new StringBuilder();

            sb.Append("namespace Gameready.Gameplay.SoundsManager.Data");
            sb.AppendLine("{");
            sb.AppendLine($"    public static class {_className}");
            sb.AppendLine("    {");

            foreach (var clipName in _clipNames)
            {
                if (_soundIds.Contains(clipName))
                {
                    continue;
                }


                sb.AppendLine(
                    $@"        public const string {CreateConstName(clipName)} = ""{clipName}"";");

                _soundIds.Add(clipName);

                // if (!string.IsNullOrEmpty(soundInfo.id2))
                // {
                //     sb.AppendLine(
                //         $@"        public const string {CreateConstName(soundInfo.id2)} = ""{soundInfo.id}"";");
                //     _soundIds.Add(soundInfo.id2);
                // }
            }

            sb.AppendLine("    }");
            sb.AppendLine("}");

            return sb.ToString();
        }

        private string CreateConstName(string id)
        {
            var chars = id.ToCharArray();
            chars[0] = char.ToUpper(chars[0]);
            return new string(chars);
        }
    }
}