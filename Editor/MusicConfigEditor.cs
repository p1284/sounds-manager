using Gameready.Gameplay.SoundsManager.Editor;
using Gameready.Gameplay.SoundsManager.Music;
using UnityEditor;

namespace Gameplay.SoundsManager.Editor
{
    [CustomEditor(typeof(MusicConfig))]
    public class MusicConfigEditor : BaseSoundsConfigEditor
    {
        private MusicConfig _config;

        protected override void OnEnable()
        {
            base.OnEnable();
            _config = (MusicConfig)target;
        }

        protected override void PlayWithParams(string id)
        {
            base.PlayWithParams(id);
        }
    }
}