using System.Linq;
using Gameready.Gameplay.SoundsManager.Data;
using UnityEditor;
using UnityEngine;

namespace Gameready.Gameplay.SoundsManager.Editor
{
    [CustomEditor(typeof(SoundsConfig))]
    public class SoundsConfigEditor : BaseSoundsConfigEditor
    {
        private SoundsConfig _config;

        private ISfxManager _sfxManager;

        protected override void OnEnable()
        {
            base.OnEnable();
            _config = (SoundsConfig)target;

            if (Application.isPlaying)
            {
                _sfxManager = GameObject.FindObjectOfType<SfxManager>();
            }
        }

        protected override void PlayWithParams(string id)
        {
            _sfxManager?.Play(id);
        }
    }
}