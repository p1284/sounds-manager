using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Gameready.Gameplay.SoundsManager.Editor
{
    [CustomEditor(typeof(BaseSoundsConfig), true)]
    public abstract class BaseSoundsConfigEditor : UnityEditor.Editor
    {
        private BaseSoundsConfig _baseConfig;

        protected virtual void OnEnable()
        {
            _baseConfig = (BaseSoundsConfig)target;
        }

        private string[] _playList;

        private int _selectedClipIndex = 0;


        public override void OnInspectorGUI()
        {
            if (Application.isPlaying && GUILayout.Button("FillPlayList"))
            {
                FillPlayList(_baseConfig.Clips);
            }

            if (Application.isPlaying && _playList != null && _playList.Length > 0)
            {
                _selectedClipIndex = EditorGUILayout.Popup("PlayList", _selectedClipIndex, _playList);
                if (GUILayout.Button("PlayWithParams"))
                {
                    var id = _playList[_selectedClipIndex];
                    PlayWithParams(id);
                }
            }

            if (GUILayout.Button("Generate SoundIDs"))
            {
                GenerateSoundIdsClass(_baseConfig.generateClassName, _baseConfig.Clips);
            }

            base.OnInspectorGUI();
        }

        protected virtual void PlayWithParams(string id)
        {
        }


        private void GenerateSoundIdsClass(string generateClassName, List<AudioClip> clips)
        {
            if (!string.IsNullOrEmpty(generateClassName))
            {
                var path = EditorUtility.SaveFilePanel($"Save {generateClassName}",
                    Application.dataPath, generateClassName, "cs");

                if (!string.IsNullOrEmpty(path))
                {
                    var classData =
                        new SoundIdsGenerator(generateClassName, clips.Select(clip => clip.name).ToList()).Generate();

                    File.WriteAllText(path, classData, Encoding.UTF8);

                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                }
            }
            else
            {
                Debug.LogError("[SoundsManager] BaseSoundsConfig has empty generateClassName");
            }
        }

        private void FillPlayList(List<AudioClip> clips)
        {
            var list = new List<string>();
            foreach (var clip in clips)
            {
                list.Add(clip.name);
            }

            _playList = list.ToArray();
        }
    }
}