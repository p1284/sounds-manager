using Gameready.Gameplay.SoundsManager.Data;
using UnityEngine;

namespace Gameready.Gameplay.SoundsManager
{
    [CreateAssetMenu(fileName = "SoundsConfig", menuName = "SoundsManager/SfxConfig", order = 0)]
    public class SoundsConfig : BaseSoundsConfig,ISoundsConfig
    {
        public SoundModel[] soundsList;
        
        public override void Initialize()
        {
            foreach (var soundModel in soundsList)
            {
                AddModel(soundModel);
            }
        }

        public SoundModel GetModel(string id)
        {
            return GetModelInternal<SoundModel>(id);
        }
    }
}