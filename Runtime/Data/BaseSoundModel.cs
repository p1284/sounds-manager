using System;
using UnityEngine;

namespace Gameready.Gameplay.SoundsManager.Data
{
    [Serializable]
    public abstract class BaseSoundModel
    {
        [SerializeField] private string _soundId;
        
        [Range(0f,1f)][SerializeField] private float _startVolume = 1f;
        
        public string SoundId => _soundId;
        
        public float StartVolume => _startVolume;

        public abstract void ResetToDefault();
    }
}