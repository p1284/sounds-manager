using System;
using UnityEngine;

namespace Gameready.Gameplay.SoundsManager.Data
{
    [Serializable]
    public class MusicModel : BaseSoundModel, ISerializationCallbackReceiver
    {
        [NonSerialized] public float totalTime;
        [NonSerialized] public float currentTime = 0f;
        [NonSerialized] public float volume;
        
        [NonSerialized] public float updateTime = 0f;

        [SerializeField] private bool _loop;
        
        public bool Loop => _loop;

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            ResetToDefault();
        }

        public override void ResetToDefault()
        {
            volume = StartVolume;
            updateTime = 0f;
            totalTime = 0;
            currentTime = 0f;
        }
    }
}