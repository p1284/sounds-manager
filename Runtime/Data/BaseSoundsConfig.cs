using System;
using System.Collections.Generic;
using Gameready.Gameplay.SoundsManager.Data;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Gameready.Gameplay.SoundsManager
{
    public abstract class BaseSoundsConfig : ScriptableObject
    {
#if UNITY_EDITOR

        [Header("Editor Only")] public string generateClassName;

        public List<AudioClip> Clips => _clips;

#endif

        [Header("Addressables preload Label")] public AssetLabelReference preloadLabel;

        private Dictionary<string, BaseSoundModel> _dictionary;

        [SerializeField] private List<AudioClip> _clips;


        public AudioClip GetClip(string id)
        {
            return _clips.Find(clip => clip.name == id);
        }

        public void AddClip(AudioClip clip)
        {
            _clips.Add(clip);
        }

        public abstract void Initialize();

        protected void AddModel(BaseSoundModel model)
        {
            _dictionary ??= new Dictionary<string, BaseSoundModel>();
            if (!_dictionary.ContainsKey(model.SoundId))
            {
                ValidateClipNames(model.SoundId);
                _dictionary.Add(model.SoundId, model);
            }
            else
            {
                Debug.LogError($"[SoundsManager] duplicate model id:{model.SoundId}");
            }
        }

        private void ValidateClipNames(string id)
        {
            for (var i = 0; i < _clips.Count; i++)
            {
                var audioClip = _clips[i];
                if (audioClip.name == id)
                {
                    return;
                }
            }

            if (_clips.Count > 0)
                Debug.LogError($"[SoundsManager] clip with id:{id} not found!");
        }

        public bool ContainsModel(string id)
        {
            return _dictionary.ContainsKey(id);
        }

        protected T GetModelInternal<T>(string id) where T : BaseSoundModel
        {
            if (_dictionary.TryGetValue(id, out var value))
            {
                return (T)value;
            }

            throw new Exception($"[SoundsManager] model with id:{id} not exist");
        }
    }
}