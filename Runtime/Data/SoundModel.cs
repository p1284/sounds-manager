using System;
using UnityEngine;

namespace Gameready.Gameplay.SoundsManager.Data
{
    [Serializable]
    public class SoundModel : BaseSoundModel, ISerializationCallbackReceiver
    {
        [NonSerialized] public float volume = 1f;

        [SerializeField] private float _startPitch = 1;

        [NonSerialized] public float pitch;

        public float StartPitch => _startPitch;


        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            ResetToDefault();
        }

        public override void ResetToDefault()
        {
            volume = StartVolume;
            pitch = _startPitch;
        }
    }
}