using System;
using UnityEngine;

namespace Gameready.Gameplay.SoundsManager.Data
{
    [Serializable]
    public class AudioSourceModel
    {
        public AudioSource source;

        public string soundId;

    }
}