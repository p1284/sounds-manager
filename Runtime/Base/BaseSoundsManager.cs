using System;
using System.Collections;
using System.Collections.Generic;
using Gameready.Gameplay.SoundsManager.Data;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Gameready.Gameplay.SoundsManager
{
    public abstract class BaseSoundsManager : MonoBehaviour
    {
        private const string SoundsMuteStatePrefKey = "soundsmanager_mutestate_prefkey_";

        [SerializeField] private bool _cacheMuteStates;

        [SerializeField] protected List<AudioSourceModel> _poolSources;

        protected virtual void Awake()
        {
            LoadMuteState();
        }

        private void LoadMuteState()
        {
            var managerType = GetType().Name.ToLower();
            Mute = _cacheMuteStates &&
                   Convert.ToBoolean(PlayerPrefs.GetInt($"{SoundsMuteStatePrefKey}{managerType}", 0));
        }

        private bool _mute;

        public abstract void StartPreload(Action callback);

        public abstract void Play(string id);

        public virtual void Stop(string id)
        {
            foreach (var sourceModel in _poolSources)
            {
                if (sourceModel.source.isPlaying && sourceModel.soundId == id)
                {
                    sourceModel.source.Stop();
                }
            }
        }

        public virtual void StopAll()
        {
            foreach (var sourceModel in _poolSources)
            {
                sourceModel.source.Stop();
            }
        }

        public bool Mute
        {
            get => _mute;
            set
            {
                _mute = value;
                MuteInternal(_mute);
            }
        }

        protected virtual void MuteInternal(bool mute)
        {
            foreach (var sourceModel in _poolSources)
            {
                sourceModel.source.mute = mute;
            }

            if (_cacheMuteStates)
            {
                var managerType = GetType().Name.ToLower();
                PlayerPrefs.SetInt($"{SoundsMuteStatePrefKey}{managerType}", Convert.ToInt32(_mute));
                PlayerPrefs.Save();
            }
        }

        protected AudioSourceModel GetOrCreateSource(GameObject sourcePrefab)
        {
            foreach (var sourceModel in _poolSources)
            {
                if (!sourceModel.source.isPlaying)
                {
                    return sourceModel;
                }
            }

            var audioSource = Instantiate(sourcePrefab, transform).GetComponent<AudioSource>();

            var source = new AudioSourceModel()
            {
                source = audioSource
            };

            _poolSources.Add(source);

            return source;
        }

        protected IEnumerator StartPreloadRoutine(BaseSoundsConfig config, Action callback)
        {
            var label = config.preloadLabel;
            var handle = Addressables.LoadAssetsAsync<AudioClip>(label.labelString, null);

            while (!handle.IsDone)
            {
                yield return null;
            }

            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                foreach (var audioClip in handle.Result)
                {
                    config.AddClip(audioClip);
                }
            }

            callback?.Invoke();
        }
    }
}