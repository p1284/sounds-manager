using System;

namespace Gameready.Gameplay.SoundsManager
{
    public interface ISoundsManager
    {
        void Play(string id);

        void Stop(string id);

        void StopAll();

        bool Mute { get; set; }
        
        void StartPreload(Action callback);
    }
}