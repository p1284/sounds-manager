using Gameready.Gameplay.SoundsManager.Data;

namespace Gameready.Gameplay.SoundsManager
{
    public interface ISoundsConfig
    {
        SoundModel GetModel(string id);
    }
}