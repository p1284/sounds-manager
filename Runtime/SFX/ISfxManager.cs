using Gameready.Gameplay.SoundsManager.Data;

namespace Gameready.Gameplay.SoundsManager
{
    public interface ISfxManager:ISoundsManager
    {
        void Play(SoundModel model);

        void PlayOneShotSound(string id, float volumeScale = 1f);
        
        ISoundsConfig Config { get; }
        
    }
}