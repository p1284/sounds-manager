using System;
using Gameready.Gameplay.SoundsManager.Data;
using UnityEngine;

namespace Gameready.Gameplay.SoundsManager
{
    /// <summary>
    /// Sfx Manager
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class SfxManager : BaseSoundsManager, ISfxManager
    {
        [SerializeField] private SoundsConfig _config;

        [SerializeField] private AudioSource _oneShotSource;

        public ISoundsConfig Config => _config;

        protected override void Awake()
        {
            base.Awake();
            _config.Initialize();
        }

        private void Update()
        {
            foreach (var sourceModel in _poolSources)
            {
                if (!sourceModel.source.isPlaying || _config.ContainsModel(sourceModel.soundId)) continue;
                var model = _config.GetModel(sourceModel.soundId);
                sourceModel.source.pitch = model.pitch;
                sourceModel.source.volume = model.volume;
            }
        }

        public void Play(SoundModel model)
        {
            PlaySoundInternal(model);
        }

        public override void StartPreload(Action callback)
        {
            StartCoroutine(StartPreloadRoutine(_config, callback));
        }

        public override void Play(string id)
        {
            var model = _config.GetModel(id);
            PlaySoundInternal(model);
        }

        public void PlayOneShotSound(string id, float volumeScale = 1f)
        {
            _oneShotSource.PlayOneShot(_config.GetClip(id), volumeScale);
        }


        public override void StopAll()
        {
            _oneShotSource.Stop();
            base.StopAll();
        }

        protected override void MuteInternal(bool mute)
        {
            _oneShotSource.mute = mute;
            base.MuteInternal(mute);
        }

        private void PlaySoundInternal(SoundModel model)
        {
            var source = GetOrCreateSource(_oneShotSource.gameObject);
            source.source.clip = _config.GetClip(model.SoundId);
            source.source.volume = model.volume;
            source.source.pitch = model.pitch;
            source.soundId = model.SoundId;

            source.source.gameObject.name = model.SoundId;
            source.source.Play();
        }
    }
}