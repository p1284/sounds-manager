using UnityEngine;

namespace Gameready.Gameplay.SoundsManager
{
    public static class SoundUtils
    {
        public static float GetRandomPitch(float from,float offset)
        {
            return from + Random.Range(-offset, offset);
        }
    }
}