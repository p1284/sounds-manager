using Gameready.Gameplay.SoundsManager.Data;
using UnityEngine;

namespace Gameready.Gameplay.SoundsManager.Music
{
    [CreateAssetMenu(fileName = "MusicConfig", menuName = "SoundsManager/MusicConfig", order = 0)]
    public class MusicConfig : BaseSoundsConfig,IMusicConfig
    {
        public MusicModel[] musicList;

        public override void Initialize()
        {
            foreach (var musicModel in musicList)
            {
                AddModel(musicModel);
            }
        }
        

        public MusicModel GetModel(string id)
        {
            return GetModelInternal<MusicModel>(id);
        }
    }
}