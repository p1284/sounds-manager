using Gameready.Gameplay.SoundsManager.Data;

namespace Gameready.Gameplay.SoundsManager.Music
{
    public interface IMusicConfig
    {
        MusicModel GetModel(string id);
    }
}