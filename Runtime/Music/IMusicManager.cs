using System;
using Gameready.Gameplay.SoundsManager.Data;

namespace Gameready.Gameplay.SoundsManager.Music
{
    public interface IMusicManager : ISoundsManager
    {
        void Play(MusicModel model);

        void Pause(string id, bool pause);

        string CurrentPlayId { get; }

        MusicModel GetCurrentPlayModel();
    }
}