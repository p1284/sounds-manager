using System;
using Gameready.Gameplay.SoundsManager.Data;
using UnityEngine;

namespace Gameready.Gameplay.SoundsManager.Music
{
    /// <summary>
    /// Music Manager
    /// </summary>
    public class MusicManager : BaseSoundsManager, IMusicManager
    {
        [SerializeField] private MusicConfig _config;

        [SerializeField] private GameObject _audioSourcePrefab;

        private string _currentPlayId;
        public string CurrentPlayId => _currentPlayId;

        public IMusicConfig Config => _config;

        protected override void Awake()
        {
            base.Awake();
            _config.Initialize();
        }

        private void Update()
        {
            foreach (var sourceModel in _poolSources)
            {
                if (!sourceModel.source.isPlaying || !_config.ContainsModel(sourceModel.soundId)) continue;
                var model = _config.GetModel(sourceModel.soundId);
                sourceModel.source.volume = model.volume;
                if (model.updateTime > 0 && model.updateTime < model.totalTime)
                {
                    sourceModel.source.time = model.updateTime;
                    model.updateTime = 0f;
                }
                
                model.currentTime = sourceModel.source.time;
            }
        }

        public void Pause(string id, bool pause)
        {
            foreach (var sourceModel in _poolSources)
            {
                if (sourceModel.soundId == id)
                {
                    if (pause)
                    {
                        sourceModel.source.Pause();
                    }
                    else
                    {
                        sourceModel.source.UnPause();
                    }
                }
            }
        }

        public MusicModel GetCurrentPlayModel()
        {
            if (!string.IsNullOrEmpty(_currentPlayId))
            {
                return _config.GetModel(_currentPlayId);
            }

            return null;
        }

        public void Play(MusicModel model)
        {
            PlayMusicInternal(model);
        }

        public override void StartPreload(Action callback)
        {
            StartCoroutine(StartPreloadRoutine(_config, callback));
        }

        public override void Stop(string id)
        {
            base.Stop(id);
            _currentPlayId = null;
            foreach (var musicModel in _config.musicList)
            {
                if (musicModel.SoundId == id)
                {
                    musicModel.ResetToDefault();
                }
            }
        }
        
        public override void StopAll()
        {
            base.StopAll();
            _currentPlayId = null;
            foreach (var musicModel in _config.musicList)
            {
                musicModel.ResetToDefault();
            }
        }
        
        public override void Play(string id)
        {
            var model = _config.GetModel(id);
            PlayMusicInternal(model);
        }

        private void PlayMusicInternal(MusicModel model)
        {
            _currentPlayId = model.SoundId;

            var source = GetOrCreateSource(_audioSourcePrefab);
            source.source.time = model.currentTime;
            
            source.source.clip = _config.GetClip(model.SoundId);
            model.totalTime = source.source.clip.length;
            model.currentTime = 0f;

            source.source.volume = model.volume;
            source.source.loop = model.Loop;
            source.soundId = model.SoundId;

            source.source.Play();
        }
    }
}